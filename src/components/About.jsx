import React from "react";
import "./About.scss";

const About = ({ hero }) => {
  return (
    <div className="abou">
      <div className="about-card">
        {hero.map((item) => {
          return (
            <div key={JSON.stringify(item)}>
              <p className="name">📕 {item.info}</p>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default About;

