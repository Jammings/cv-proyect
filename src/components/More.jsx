import React from "react";
import "./More.scss";


const More = ({ languages, habilities }) => {
  return (

    <div className="more">
      <div className="more-card">
      {languages.map((item) => {
        return ( 
        <div key={JSON.stringify(item)}>
              <p className="name">📕 {item.language}</p>
              <p>{item.wrlevel}</p>
            </div>
  
        );
        })}
      </div>
      </div>
  );
};




export default More;