
import "./Education.scss";



const Education = ({ education }) => {
  return (
    <div className="expe">
      <div className="experience-card">
        {education.map((item) => {
          return (
            <div key={JSON.stringify(item)}>
              <p className="name">📕 {item.name}</p>
              <p>{item.where}</p>
              <p>{item.languages}</p>
              <p>{item.date}</p>
            </div>
          );
        })}
      </div>
    </div>
  );
};


export default Education;