export const CV = {
    hero: {
      name: "Ricardo",
      adress: "Laurens Lara",
      city: "Logroño / La Rioja",
      email: "RL.93@hotmail.com",
      birthDate: "12/01",
      image: "https://res.cloudinary.com/dulzlnv9t/image/upload/v1660582758/Capa_4_dvqarf.png",
      gitHub: "https://github.com/Jammings",
      aboutMe: [
        {
          info: "Organizado, atento, responsable, con excelentes habilidades de comunicación,  alto interés  en la tecnología y capacidad   para resolver problemas de manera eficiente .",
        },
       /*  {
          info: "🔩 CEO of Stark Industries.",
        },
        {
          info: "🕶 Genius, billionaire, playboy, philanthropist.",
        },
        {
          info: "🦾 I do have a responsibility to keep my inventions from evil hands – but I have a greater responsibility to oppose that evil any way I can.",
        }, */
      ],
    },
    education: [
      {
        name: "Full Stack developer BootCamp",
        date: "2022",
        where: "Upgrade-Hub",
        languages: "Html5 - CSS3 - JavaScript - Node.js - Git - BBDD NoSQL - BBDD SQL - PHP - React - Angular ",
      },
      {
        name: "MasterClass de fotografía y video profesional con el movíl",
        date: "2022",
        where: "Ponentes: SHOTBYMARCOS - DELAQUADRA",
      },
      {
        name: "Curso avanzado Adobe  Photoshop",
        date: "2020",
        where: "Domestika",
      },
      {
        name: "Fotografía móvil y productos para Instagram",
        date: "2020",
        where: "Domestika",
      },
      {
        name: "Curso Básico Adobe Illustrator",
        date: "2020",
        where: "Domestika",
      },
      {
        name: "Lic. Psicología Industrial",
        date: "2017",
        where: "Universidad Bicentenaria de Aragua",
      },
    ],
    experience: [
      {
        name: "Creador de contenido",
        date: "Abril 2019 - Actual",
        where: "Instagram: Que.cuqui",
        description:[         
        "- Gestión y diseño de redes sociales.",
        "- Diseño  y redacción de publicaciones.",
        "- Fotografía para productos.",
        "- Creación de contenido gráfico.",]
      },
      {
        name:  "Supervisor",
        date:  "Mayo 2018 - Agosto 2021",
        where: "EcoXpress",
        description:
          "- Supervisión de flota de reparto en moto. - Control de kilometraje semanal de vehículos. -Gestión de motos para revisiones en taller. -Gestión de contratos. -Recepción y entrega de vehículos a taller y trabajadores.",
      },
    ],
    languages:[  {
      language: "Spanish",
      wrlevel: "Nativo",
      splevel: "Nativo",
    },
    {
      language: "English",
      wrlevel: "Medio",
      splevel: "Medio",
    },
  ],
    habilities: [
/*       "Robotics", */
/*       "Robot Programming",
      "Physics",
      "Weaponery",
      "Engineer",
      "Money",
      "Dating",
      "Saving the world", */
    ],
      };

export default CV