import { useState } from "react";
import "./App.scss";
import Hero from "./components/Hero";
import About from "./components/About";
import Education from "./components/Education";
import Experience from "./components/Experience";
import More from "./components/More";
import { CV } from "./CV/CV";
import { useDispatch, useSelector } from 'react-redux';
import { eduButton, expButton } from "./components/redux/cv-redux/cv.action";

const { hero, education, experience, languages, habilities } = CV;

function App() {
  const dispatch = useDispatch();
  const {showEducation} = useSelector(state => state.cv);
  console.log(showEducation)
  return (
    <>
      <div className="App">
        <Hero hero={hero} />
        <About hero={hero.aboutMe} />
        <More languages={languages} habilities={habilities} />
       <button className="custom-btn btn-4" onClick={() => dispatch(eduButton())}>Education</button>
       <button className="custom-btn btn-4" onClick={() => dispatch(expButton())}>Experience</button>
       {showEducation ? <Experience experience={experience}/>  : <Education education={education} />};
      </div>
    </>
  );
}

export default App;
